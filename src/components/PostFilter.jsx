import React from 'react'
import MyInput from './UI/input/MyInput'
import MySelect from './UI/select/MySelect'

function PostFilter({filter, setFilter}) {
  return (
    <div>
        <MyInput
          value={filter.setSearchQuery}
          onChange={(e) => setFilter({...filter, query: e.target.value})}
          placeholder="Searching..."
        />
        <MySelect
          defaultValue="Sort by"
          value={filter.sort}
          onChange={selectedSort => setFilter({...filter, sort: selectedSort})}
          options={[
            { value: "title", name: "Name" },
            { value: "body", name: "Discription" },
          ]}
        />
      </div>
  )
}

export default PostFilter