import React from "react";
import "./styles/App.css";
import { BrowserRouter} from "react-router-dom";
import NavBar from "./components/UI/NavBar/NavBar.jsx";
import AppRouter from "./components/UI/AppRouter.jsx";

function App() {
  return (
    <BrowserRouter>
      <NavBar />
      <AppRouter />
    </BrowserRouter>
  );
}

export default App;
